#include <ESP8266WiFi.h>
#include "HTTPSRedirect.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "PCF8574.h"
#include <Ticker.h>
#include <ArduinoOTA.h>

#define PIN_WD_FEED D5

const char* ssid = "ssid";
const char* password = "password";
const char* GScriptId = "AKfycbxvjSTNYbhadHZEwSXvZu8rb3tjsgos5yJZ-RCowrzvGzNUagafS86PFSuCwX1mMUQs1g";

const char* hostName = "CLIMA-B";

bool buttonpressed = false;
bool validResponse = false;

// https://script.google.com/macros/s/AKfycbxvjSTNYbhadHZEwSXvZu8rb3tjsgos5yJZ-RCowrzvGzNUagafS86PFSuCwX1mMUQs1g/exec

const char* host = "script.google.com";
const char* googleRedirHost = "script.googleusercontent.com";
const int httpsPort = 443;
String url = String("/macros/s/") + GScriptId + "/exec";

const unsigned long google_check_interval = 60000;  // 60 seconds

const int antibounce = 500;

const int RELE_ON = 0;
const int RELE_OFF = 1;

bool RELE_1 = false;
bool RELE_2 = false;
bool RELE_3 = false;
bool RELE_4 = false;

bool BTN_1 = false;
bool BTN_2 = false;
bool BTN_3 = false;
bool BTN_4 = false;

bool update_display_flag = true;
bool google_check_flag = true;

String googleResponse;

WiFiClient client;
HTTPSRedirect* HTTPSclient = nullptr;
LiquidCrystal_I2C lcd(0x27, 16, 2);
PCF8574 expander(0x20);
Ticker ticker_google;
Ticker ticker_display;

void setup() {

  Serial.begin(115200);
  Serial.println("\r\n\r\n");
  Serial.println("Starting Climatic Control");

  expander.pinMode(P0, OUTPUT);
  expander.pinMode(P1, OUTPUT);
  expander.pinMode(P2, OUTPUT);
  expander.pinMode(P3, OUTPUT);

  expander.pinMode(P4, INPUT);
  expander.pinMode(P5, INPUT);
  expander.pinMode(P6, INPUT);
  expander.pinMode(P7, INPUT);

  expander.begin();
  Serial.println("Expander inizializzato");

  expander.digitalWrite(P0, RELE_OFF);
  expander.digitalWrite(P1, RELE_OFF);
  expander.digitalWrite(P2, RELE_OFF);
  expander.digitalWrite(P3, RELE_OFF);
  Serial.println("Spengo rele");

  // Setup del pin di alimentazione Watchdog
  pinMode(PIN_WD_FEED, OUTPUT);
  digitalWrite(PIN_WD_FEED, 0);

  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("   CONTROLLO    ");
  lcd.setCursor(0, 1);
  lcd.print("     CLIMA      ");

  start_wifi_connection();

  // Aggiorna il display ogni 2 sec
  ticker_display.attach(2, update_display_set_flag);
  // Controlla Google ogni 60 sec
  ticker_google.attach(60, google_check_set_flag);

  lcd.clear();

  init_OTA();
}

void loop() {

  // Alimenta il WatchDog
  digitalWrite(PIN_WD_FEED, 1);
  delay(10);
  digitalWrite(PIN_WD_FEED, 0);

  ArduinoOTA.handle();

  if (google_check_flag) {
    Serial.println("Check google script...");
    lcd.setCursor(0, 1);
    lcd.print("Checking Google  ");
    if (WiFi.status() != WL_CONNECTED) start_wifi_connection();
    if (!client.connected()) client.connect(host, httpsPort);
    Serial.println(host);
    Serial.println(url);
    Serial.println(googleRedirHost);

    HTTPSclient = new HTTPSRedirect();
    HTTPSclient->setInsecure();
    HTTPSclient->setPrintResponseBody(true);

    int result = HTTPSclient->connect(host, 443);
    HTTPSclient->GET(url, host);
    googleResponse = HTTPSclient->getResponseBody();

    delete HTTPSclient;
    HTTPSclient = nullptr;

    Serial.print("result: ");
    Serial.println(googleResponse);

    validResponse = (googleResponse.indexOf("Z") >= 0) ? true : false;

    if (validResponse == true) {

      Serial.println("Risposta valida!");

      if (hostName == "CLIMA-B") {
        RELE_1 = (googleResponse.indexOf("E") > 0) ? true : false;
        RELE_2 = (googleResponse.indexOf("F") > 0) ? true : false;
        RELE_3 = (googleResponse.indexOf("G") > 0) ? true : false;
        RELE_4 = (googleResponse.indexOf("H") > 0) ? true : false;
      } else {
        RELE_1 = (googleResponse.indexOf("A") > 0) ? true : false;
        RELE_2 = (googleResponse.indexOf("B") > 0) ? true : false;
        RELE_3 = (googleResponse.indexOf("C") > 0) ? true : false;
        RELE_4 = (googleResponse.indexOf("D") > 0) ? true : false;
      }
      lcd.setCursor(0, 1);
      lcd.print(googleResponse);
      lcd.print("               ");
    } else {
      lcd.setCursor(0, 1);
      lcd.print("No response");
    }
    googleResponse = "";
    google_check_flag = false;
  }



  //  Serial.println(expander.digitalRead( P4 ) );
  //  Serial.println(expander.digitalRead( P5 ) );
  //  Serial.println(expander.digitalRead( P6 ) );
  //  Serial.println(expander.digitalRead( P7 ) );

  if (expander.digitalRead(P4)) {
    RELE_1 = !RELE_1;
    Serial.println("Pulsante 1");
    buttonpressed = true;
  }

  if (expander.digitalRead(P5) == 1) {
    RELE_2 = !RELE_2;
    Serial.println("Pulsante 2");
    buttonpressed = true;
  }

  if (expander.digitalRead(P6) == 1) {
    RELE_3 = !RELE_3;
    Serial.println("Pulsante 3");
    buttonpressed = true;
  }

  if (expander.digitalRead(P7) == 1) {
    RELE_4 = !RELE_4;
    Serial.println("Pulsante 4");
    buttonpressed = true;
  }

  if (RELE_1 == true) {
    expander.digitalWrite(P0, RELE_ON);
  } else {
    expander.digitalWrite(P0, RELE_OFF);
  }

  if (RELE_2 == true) {
    expander.digitalWrite(P1, RELE_ON);
  } else {
    expander.digitalWrite(P1, RELE_OFF);
  }

  if (RELE_3 == true) {
    expander.digitalWrite(P2, RELE_ON);
  } else {
    expander.digitalWrite(P2, RELE_OFF);
  }

  if (RELE_4 == true) {
    expander.digitalWrite(P3, RELE_ON);
  } else {
    expander.digitalWrite(P3, RELE_OFF);
  }

  if (buttonpressed == true) {
    buttonpressed = false;
    delay(antibounce);  //anti-bounce;
  }


  if (update_display_flag) {
    String row_1 = "";
    String row_2 = "                ";

    row_1 += RELE_1 ? "ON  " : "OFF ";
    row_1 += RELE_2 ? "ON  " : "OFF ";
    row_1 += RELE_3 ? "ON  " : "OFF ";
    row_1 += RELE_4 ? "ON  " : "OFF ";

    lcd.setCursor(0, 0);
    lcd.print(row_1);

    lcd.setCursor(0, 1);
    lcd.print(row_2);

    update_display_flag = false;

    Serial.println("Display");
  }



  delay(50);
}



/*********************************************************************************************************/


// Routines chiamate dal ticker
void update_display_set_flag() {
  update_display_flag = true;
}
void google_check_set_flag() {
  google_check_flag = true;
}


void start_wifi_connection() {
  byte c = 0;
  const byte timeout = 20;

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED && c < timeout) {
    delay(500);
    Serial.print(".");
    c++;
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("");
    Serial.print("Succesfully connected to: ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println(WiFi.macAddress());
    Serial.println(WiFi.subnetMask());
    Serial.println(WiFi.gatewayIP());
    Serial.println(WiFi.dnsIP());

  } else {
    Serial.println("Not connected!");
  }
}

void init_OTA() {
  ArduinoOTA.onStart([]() {
    //debugI("Inizio aggiornamento OTA");
  });
  ArduinoOTA.onEnd([]() {
    ESP.restart();
  });
  ArduinoOTA.onError([](ota_error_t error) {
    //debugI("Error[%u]: ", error);
  });

  ArduinoOTA.setHostname(hostName);
  ArduinoOTA.begin();
}

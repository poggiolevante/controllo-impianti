# 	Controllo Impianti


## Breve Descrizione

**Permette di accendere gli impianti mediante la creazione di eventi sul calendario google**. **Per accendere un determinato impianto basta intitolare un evento con la parola chiave associata (maiuscolo o minuscolo indifferente)**

## Stato attuale progetto

Viene attualmente utilizzato in Collegio, tramite due microprocessori Esp permette il controllo di fancoil, condizionatori, termosifoni e caldaia della residenza. 

### **Bug da correggere**

 1) Ogni tanto potrebbe bloccarsi su alcuni eventi google (per un problema di versione delle librerie utilizzate).  
     POSSIBILE SOLUZIONE: Aggiornare le librerie del programma e garantire un reboot dei dispositivi; sarebbe utile adibire un modulo raspberry/arduino al monitoraggio degli effettivi stati (ON-OFF) del dispositivo per informare l'utente di un eventuale mal funzionamento.

## Documentazione

https://gitlab.com/poggiolevante/controllo-impianti/-/wikis/home
